# Hasura

## Requirements

This project requires the following tools to be installed:

- [Hasura CLI](https://hasura.io/docs/latest/hasura-cli/install-hasura-cli/)
- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose Plugin](https://docs.docker.com/compose/install/)

## Initial Setup

### Initial Migration

To create an initial migration, run the following command after populating some tables within your schema. This assumes
you're only using the `public` schema - if using additional schemas, specify them as a comma separated list. Ex,
`--schema public,auth`.

```shell
hasura migrate create "init" --from-server --schema public
```

### Authentication

To utilize JWTs from Authentik for authentication to Hasura, perform the following steps.

1. Create a `Customization -> Property Mapping` in Authentik if one does not already exist.
    - Name: `Hasura Roles: 'hasura'`
    - Scope Name: `hasura`
    - Description: `Maps Hasura roles from group membership to the token in the appropriate claims object.`
    - Expression:

    ```py
    ak_logger.info("hasura roles called")
    hasura = {
      "x-hasura-default-role": "anonymous",
      "x-hasura-allowed-roles": []
    }
    
    if user:
      group_attrs = user.group_attributes()
      hasura["x-hasura-user-id"] = str(user.uuid)
      hasura["x-hasura-allowed-roles"] = [
        "anonymous",
        *group_attrs.get("x-hasura-allowed-roles", []),
      ]
    
    return {
      "https://hasura.io/jwt/claims": hasura,
    }
    ```

2. Create a new provider in Authentik.
    - Type: OAuth2/OpenId Provider
    - Name: Name of application
    - Select a relevant Authentication Flow
    - Select Implicit Authorization Flow
    - Client type: public
    - Client ID: This _can_ be left default, but it's reccomended to change to something related to the application.
    - Redirect URI: URLs of application. If using svelte kit, be sure to include `http://localhost:5173/.*` for local dev.
      - _NOTE the `.*` here - just using `*` will not work. It's recommended to include any path with `.*` on all domains.
    - Under Advanced Protocol settings:
      - In the list of scopes, ctrl-click to add the "Hasura Roles" scope created above.
        - All scopes should include: `email`, `offline_access`, `openid`, `profile`, `hasura`
      - Subject mode: "Based on User's UUID"
      - Ensure "Include claims in id_token" is enabled

3. Create a new Application in Authentik.
    - Name: Name of application
    - Slug: `slug-friendly` form of application. This will be the url used in configuration and displayed to users.
    - Provider: Select the provider created previously.
    - UI settings: Customize if desired.

4. Create a group in Authentik with role mappings.
    - Name: Something Sensible :)
    - Attributes:

    ```yml
    # default role for this group.
    # Typically, this is 'anonymous' or a low-permission role, and may be omitted at the group level.
    x-hasura-default-role: admin
    # Roles this group is allowed to assume. This will be merged with other groups the user is a member of. 
    x-hasura-allowed-roles:
      - admin
    ```

5. Test the configuration by:
    - Adding a user to the relevant group
    - Navigate to Providers -> [Provider we created] -> Preview
    - Select the user with the relevant group
    - Verify:
      - "https://hasura.io/jwt/claims" exists
      - x-hasura-default-role is populated string
      - x-hasura-user-id is populated string
      - x-hasura-allowed-roles is an array, and populated if the user has a group which adds allowed roles

6. Configure the Hasura instance to use the Authentik JWTs by updating the `HASURA_GRAPHQL_JWT_SECRET` environment
   variable in docker-compose.yml
    - You must replace `REPLACE_WITH_PROVIDER` with the slug of the application created in Authentik.
    - Example: If the application slug is `my-app`, the environment variable would be:

    ```yaml
    HASURA_GRAPHQL_JWT_SECRET: '{"allowed_skew":600, "jwk_url":"https://authentik.server/application/o/my-app/jwks/"}'`
    ```

> **NOTE**: Within your application code, be sure to request the `hasura` scope when authenticating.

## Development

1. Start the Hasura Server.

    ```shell
    docker-compose up -d
    ```

2. Apply the seed to populate tables (if seeds are present)
   - NOTE: you only need to do this when starting the project for the first time,
     or after deleting data with `docker-compose down -v`.

       ```shell
       hasura seed apply
       ```

3. Start the Hasura Console.

    ```shell
    hasura console
    ```

This will start a hasura console on your local system. Any changes to the metadata or schema made through this console
will automatically be saved to the `migrations` and `metadata` directories.

### Commiting changes

Changes made through the console will be saved to the `migrations` and `metadata` directories. These changes should be
committed and submitted through the normal MR process.

Migrations should be validated before opening an MR:

- Verify migrations contain a valid `down.sql` file.
  Hasura's console does not always generate a down migration with any content.
- Consider renaming migrations to something sensible. **Keep a timestamp prefix on the migration.**
- Consider grouping multiple minor migrations together manually.

For example, lets assume you added 2 columns to table 'alpha' and 1 column to table 'beta' for a single feature.

- You will need to create a down migration for each added column, simple consisting of a drop column statement:

    ```sql
    # down.sql
    ALTER TABLE alpha DROP COLUMN column1;
    ALTER TABLE alpha DROP COLUMN column2;
    ALTER TABLE beta DROP COLUMN column3;
    ```

- You should move all statements from the `up.sql` files to a single `up.sql` file
- You should rename the migration to briefly describe the feature. Ex, `20210923120000_add_user_schedule`

## Deploying changes to Production

> Note: `$BASE_URL` does **not** include /v1/graphql.
> Ex, `https://api.malteksolutions.com` not `https://api.malteksolutions.com/v1/graphql`

Run the following to commit the repo changes to production.

```shell
hasura metadata apply --endpoint $BASE_URL --admin-secret $HASURA_ADMIN_SECRET
hasura migrate  apply --endpoint $BASE_URL --admin-secret $HASURA_ADMIN_SECRET
```

## Updating Development

### Create Development Seed

To create a new development seed from an existing hasura instance, use the following command:

```shell
hasura seed create seed_name --from-table table_name --endpoint $BASE_URL --admin-secret $HASURA_ADMIN_SECRET
```

Each table should get its own seed file. If updating an existing seed, delete the existing sql file in the `seeds/`
directory.
