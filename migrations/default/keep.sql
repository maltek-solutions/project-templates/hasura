-- This is both a .keep file for git and
-- a NO-OP file for execution by hasura on startup.
-- It can be safely removed once a real migration is added.
SELECT 1;
