-- This is both a .keep file for git and a NO-OP file for execution by `hasura seed apply`
-- It can be safely removed once a real seed is added.
SELECT 1;
